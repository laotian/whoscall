#来电助手 Who's call#


### 软件功能 ###

* 类似于国外whoscall功能,通过搜索引擎识别陌生来电,目前主要是服务器端功能实现, 手机端仅包括 android demo
* 仅适用于中国大陆地区


### 版权声明 ###

* 本程序基于 ansj_seg 分词实现, ansj_seg具有中文分词及识别中文姓名功能,网址:https://github.com/NLPchina/ansj_seg
  本程序在此基础上完善了中文公司名称识别及行业识别,地址识别的功能
* 本程序可自由使用

### 效果演示 ###
* [演示网址](http://logomaker.com.cn:8080/)

### Author ###
tianlupan  Email:tianlupan@126.com